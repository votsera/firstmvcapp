﻿using System;
using FirstMVCApp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FirstMVCApp.DAL.EntitiesConfiguration
{
    public class OrderConfiguration: BaseEntityConfiguration<Order>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Order> builder)
        {
            builder.Property(p => p.Comment)
                .HasMaxLength(500);
            builder.Property(p => p.CustomerName)
                .HasMaxLength(20)
                .IsRequired();
            builder.Property(p => p.CustomerPhoneNumber)
                .HasMaxLength(500)
                .IsRequired();
            builder.Property(p => p.Qty)
                .HasDefaultValue(1)
                .IsRequired();
            builder.Property(p => p.Sum).IsRequired();
            builder.Property(p => p.DeliverDate).IsRequired();
            builder.Property(p => p.CreatedAt).IsRequired();

        }
        
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Order> builder)
        {
            builder
                .HasOne(b => b.Product)
                .WithMany(b => b.Orders)
                .HasForeignKey(b => b.ProductId)
                .IsRequired();
        }
    }
}