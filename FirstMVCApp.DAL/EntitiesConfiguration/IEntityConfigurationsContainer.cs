﻿using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.DAL.EntitiesConfiguration
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Product> ProductConfiguration { get; }
        IEntityConfiguration<Category> CategoryConfiguration { get; }
        IEntityConfiguration<Brand> BrandConfiguration { get; }
        IEntityConfiguration<Order> OrderConfiguration { get; }
        
    }
}
