﻿using System;

namespace FirstMVCApp.DAL.Entities
{
    public class Order: Entity
    {
        public Product Product { get; set; }
        public int ProductId { get; set; }
        public int Qty { get; set; }
        public decimal Sum { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime DeliverDate { get; set; }
    }
}