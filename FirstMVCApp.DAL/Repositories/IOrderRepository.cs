﻿using System.Collections.Generic;
using ConsoleAppWithDb.Repositories;
using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.DAL.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {
        IEnumerable<Order> GetAllWithProduct(decimal sumFrom, decimal sumTo);
        IEnumerable<Order> GetAllWithProduct();
    }
}