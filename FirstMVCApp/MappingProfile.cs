﻿using System;
using AutoMapper;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models;

namespace FirstMVCApp
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateProductToProductModelMap();
            CreateProductCreateModelToProduct();
            CreateBrandToBrandModelMap();
            CreateBrandCreateModelToBrand();
            CreateCategoryToCategoryModelMap();
            CreateCategoryCreateModelToCategory();
            CreateOrderToOrderModelMap();
            CreateOrderCreateModelToOrder();
        }

        private void CreateProductToProductModelMap()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(target => target.BrandName,
                    src => src.MapFrom
                        (p => p.BrandId == null ? ProductModel.NoBrand : p.Brand.Name))
                .ForMember(target => target.CategoryName,
                    src => src.MapFrom
                        (p => p.Category.Name));
        }
        private void CreateProductCreateModelToProduct()
        { 
            CreateMap<ProductCreateModel, Product>();
        }


        private void CreateBrandCreateModelToBrand()
        {
            CreateMap<BrandCreateModel, Brand>();
        }
        private void CreateBrandToBrandModelMap()
        { 
            CreateMap<Brand, BrandModel>();
        }
        
        
        private void CreateCategoryCreateModelToCategory()
        {
            CreateMap<CategoryCreateModel, Category>();
        }
        private void CreateCategoryToCategoryModelMap()
        {
            CreateMap<Category, CategoryModel>();
        }
        
        
        private void CreateOrderToOrderModelMap()
        {
            CreateMap<Order, OrderModel>()
                .ForMember(target => target.ProductName,
                    src => src.MapFrom
                        (o => o.Product.Name));

        }
        private void CreateOrderCreateModelToOrder()
        {
            CreateMap<OrderCreateModel, Order>();

        }
    }
}
