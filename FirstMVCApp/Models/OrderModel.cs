﻿using System;

namespace FirstMVCApp.Models
{
    public class OrderModel
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public int Qty { get; set; }
        public decimal Sum { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime DeliverDate { get; set; }
    }
}