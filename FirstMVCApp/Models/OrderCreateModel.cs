﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.Models
{
    public class OrderCreateModel
    {
        [Display(Name = "Выберите продукт")]
        public int ProductId { get; set; }
        
        [Display(Name = "Имя заказчика")]
        [Required(ErrorMessage = "Имя заказчика обязательно для заполнения")]
        public string CustomerName { get; set; }
        
        [Display(Name = "Номер телефона")]
        [Required(ErrorMessage = "Без вашего номера мы не сможем с вами связаться!")]
        public string CustomerPhoneNumber { get; set; }

        [Display(Name = "Количество")]
        [Required(ErrorMessage = "Количество должно быть указано")]
        [Range(0, int.MaxValue, ErrorMessage = "Количество не может быть отрицательным")]
        public int Qty { get; set; }
        
        
        [Display(Name = "Желаемая дата доставки")]
        [Required(ErrorMessage = "Без данного поля мы не доставимвам товар никогда")]
        public DateTime DeliverDate { get; set; }
        
        [Display(Name = "Комментарий")]
        public string Comment { get; set; }
        
        public SelectList ProductSelect { get; set; }
        
        public DateTime CreatedAt { get; set; }
        public decimal Sum { get; set; }

    }
}