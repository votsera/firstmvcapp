﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.Models
{
    public class ProductCreateModel
    {
        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Наименование должно быть заполненным")]
        public string Name { get; set; }

        [Display(Name = "Категория")]
        [Required(ErrorMessage = "Категория должнa быть указана")]
        public int CategoryId { get; set; }

        [Display(Name = "Брэнд")]
        public int? BrandId { get; set; }

        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Цена должно быть указана")]
        [Range(0, Double.PositiveInfinity, ErrorMessage = "Цена не может быть отрицательной")]
        public decimal Price { get; set; }
        public SelectList CategoriesSelect { get; set; }
        public SelectList BrandsSelect { get; set; }
    }
}
