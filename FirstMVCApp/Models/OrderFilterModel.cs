﻿using System;

namespace FirstMVCApp.Models
{
    public class OrderFilterModel
    {
        public string ProductName { get; set; }
        public decimal Sum { get; set; }
        public decimal? SumFrom { get; set; }
        public decimal? SumTo { get; set; }
        public string CustomerName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime DeliverDate { get; set; }
        public int? ProductId { get; set; }
    }
}