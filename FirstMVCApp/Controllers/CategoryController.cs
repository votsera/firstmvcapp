﻿using System;
using FirstMVCApp.Models;
using FirstMVCApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{
    public class CategoryController : Controller
    {
        // DI - dependency injection
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            if(categoryService == null)
                throw new ArgumentNullException(nameof(categoryService));

            _categoryService = categoryService;
        }
        
        //action
        [Route("Category/Search/{name?}")]
        public IActionResult Index([FromRoute]CategoryFilterModel model)
        {
            try
            {
                var models = _categoryService.SearchCategories(model);

                return View(models);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        public IActionResult Create()
        {
            var model = _categoryService.GetCategoryCreateModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(CategoryCreateModel model)
        {
            try
            {
                _categoryService.CreateCategory(model);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}