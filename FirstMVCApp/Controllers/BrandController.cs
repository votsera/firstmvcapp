﻿using System;
using FirstMVCApp.Models;
using FirstMVCApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{
    public class BrandController : Controller
    {
        // DI - dependency injection
        private readonly IBrandService _brandService;

        public BrandController(IBrandService brandService)
        {
            if(brandService == null)
                throw new ArgumentNullException(nameof(brandService));

            _brandService = brandService;
        }
        
        //action
        [Route("Brand/Search/{name?}")]
        public IActionResult Index([FromRoute]BrandFilterModel model)
        {
            try
            {
                var models = _brandService.SearchBrands(model);

                return View(models);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        public IActionResult Create()
        {
            var model = _brandService.GetBrandCreateModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(BrandCreateModel model)
        {
            try
            {
                _brandService.CreateBrand(model);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}