﻿using System;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models;
using FirstMVCApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            if (orderService == null)
                throw new ArgumentNullException(nameof(orderService));

            _orderService = orderService;
        }

        //action
        [Route("Order/Search/{name?}/{sumFrom?}/{sumTo?}")]
        public IActionResult Index([FromRoute] OrderFilterModel model)
        {
            try
            {
                var models = _orderService.SearchOrders(model);

                return View(models);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        public IActionResult Create()
        {
            var model = _orderService.GetOrderCreateModel();

            return View(model);
        }

        public IActionResult CreateWithProduct(int productId)
        {
            var model = _orderService.GetOrderCreateModel(productId);

            return View(model);
        }

        [HttpPost]
        public IActionResult Create(OrderCreateModel model)
        {
            try
            {
                _orderService.CreateOrder(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}