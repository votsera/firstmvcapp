﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.Services
{
    public class OrderService: IOrderService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public OrderService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }
        
        public List<OrderModel> SearchOrders(OrderFilterModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                IEnumerable<Order> orders = unitOfWork.Orders.GetAllWithProduct();
                
                orders = orders
                    .BySumFrom(model.SumFrom)
                    .BySumTo(model.SumTo)
                    .ByCustomerName(model.CustomerName)
                    .ByProductId(unitOfWork, model.ProductId);

                List<OrderModel> orderModels = Mapper.Map<List<OrderModel>>(orders);
                
                return orderModels;
            }
        }

        public OrderCreateModel GetOrderCreateModel()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var products = unitOfWork.Products.GetAll().ToList();

                return new OrderCreateModel()
                {
                    ProductSelect = new SelectList(products, nameof(Product.Id), nameof(Product.Name))
                };
            }
        }

        public void CreateOrder(OrderCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var price = unitOfWork.Products.GetById(model.ProductId).Price;
                model.Sum = model.Qty * price;
                model.CreatedAt = DateTime.Now;
                var order = Mapper.Map<Order>(model);
                unitOfWork.Orders.Create(order);
            }
        }

        public OrderCreateModel GetOrderCreateModel(int productId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var products = unitOfWork.Products.GetAll().ToList();

                int id = productId;
                return new OrderCreateModel()
                {
                    ProductSelect = new SelectList(products, nameof(Product.Id), nameof(Product.Name)),
                    ProductId = id
                };
                
            }
        }
    }
}