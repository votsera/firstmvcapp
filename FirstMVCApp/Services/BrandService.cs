﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.Services
{
    public class BrandService : IBrandService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public BrandService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<BrandModel> SearchBrands(BrandFilterModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                IEnumerable<Brand> brands = unitOfWork.Brands.GetAll();

                brands = brands.ByName(model.Name);

                List<BrandModel> brandModels = Mapper.Map<List<BrandModel>>(brands);
                
                return brandModels;
            }
        }

        public BrandCreateModel GetBrandCreateModel()
        {
            return new BrandCreateModel();
        }

        public void CreateBrand(BrandCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var brand = Mapper.Map<Brand>(model);
                unitOfWork.Brands.Create(brand);
            }
        }
    }
}
