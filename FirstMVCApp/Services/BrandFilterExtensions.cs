﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.Services
{
    public static class BrandFilterExtensions
    {
        public static IEnumerable<Brand> ByName(this IEnumerable<Brand> brands, string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
                brands = brands.Where(p => p.Name.Contains(name));
            return brands;
        }
    }
}