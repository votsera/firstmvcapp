﻿using System.Collections.Generic;
using FirstMVCApp.Models;

namespace FirstMVCApp.Services
{
    public interface IBrandService
    {
        List<BrandModel> SearchBrands(BrandFilterModel model);
        BrandCreateModel GetBrandCreateModel();
        void CreateBrand(BrandCreateModel model);
    }
}