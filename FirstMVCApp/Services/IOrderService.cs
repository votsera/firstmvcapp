﻿using System.Collections.Generic;
using FirstMVCApp.Models;

namespace FirstMVCApp.Services
{
    public interface IOrderService
    {
        List<OrderModel> SearchOrders(OrderFilterModel model);
        OrderCreateModel GetOrderCreateModel();
        OrderCreateModel GetOrderCreateModel(int productId);
        void CreateOrder(OrderCreateModel model);
        
    }
}