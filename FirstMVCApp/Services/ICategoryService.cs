﻿using System.Collections.Generic;
using FirstMVCApp.Models;

namespace FirstMVCApp.Services
{
    public interface ICategoryService
    {
        List<CategoryModel> SearchCategories(CategoryFilterModel model);
        CategoryCreateModel GetCategoryCreateModel();
        void CreateCategory(CategoryCreateModel model);
    }
}