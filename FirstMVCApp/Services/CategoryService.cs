﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public CategoryService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<CategoryModel> SearchCategories(CategoryFilterModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                IEnumerable<Category> categories = unitOfWork.Categories.GetAll();
                categories = categories.ByName(model.Name);
                List<CategoryModel> categoryModels = Mapper.Map<List<CategoryModel>>(categories);
                return categoryModels;
            }
        }

        public CategoryCreateModel GetCategoryCreateModel()
        {
            return new CategoryCreateModel();
        }

        public void CreateCategory(CategoryCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var category = Mapper.Map<Category>(model);
                unitOfWork.Categories.Create(category);
            }
        }
    }
}