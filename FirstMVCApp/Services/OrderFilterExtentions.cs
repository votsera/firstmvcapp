﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.Services
{
    public static class OrderFilterExtentions
    {
        public static IEnumerable<Order> BySumFrom(this IEnumerable<Order> orders, decimal? sumFrom)
        {
            if (sumFrom.HasValue)
                return orders.Where(o => o.Sum >= sumFrom.Value);
            return orders;
        }

        public static IEnumerable<Order> BySumTo(this IEnumerable<Order> orders, decimal? sumTo)
        {
            if (sumTo.HasValue)
                return orders.Where(o => o.Sum <= sumTo.Value);
            return orders;
        }

        public static IEnumerable<Order> ByCustomerName(this IEnumerable<Order> orders, string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
                orders = orders.Where(p => p.CustomerName.Contains(name));
            return orders;
        }

        public static IEnumerable<Order> ByProductId(this IEnumerable<Order> orders, UnitOfWork unitOfWork, int? productId)
        {
            if (productId.HasValue)
            {
                var product = unitOfWork.Products.GetById(productId.Value);
                if (product == null)
                    throw new ArgumentOutOfRangeException(
                        nameof(productId),
                        $"No product with Id {productId}");
                return orders.Where(o => o.ProductId == productId);
            }

            return orders;
        }
    }
}