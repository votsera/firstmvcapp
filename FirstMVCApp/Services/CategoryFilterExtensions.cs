﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.Services
{
    public static class CategoryFilterExtensions
    {
        public static IEnumerable<Category> ByName(this IEnumerable<Category> categories, string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
                categories = categories.Where(p => p.Name.Contains(name));
            return categories;
        }
    }
}